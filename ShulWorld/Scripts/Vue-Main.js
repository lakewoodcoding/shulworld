﻿new Vue({
    el: '#app',
    data: {
        firstName: '',
        lastName: '',
        middleName: '',
        dob: '',
        email: '',
        homePhone: '',
        altPhone1: '',
        altPhone2: '',
        address: '',
        city: '',
        state: '',
        zipcode: '',
        gender: '',
        successAlert: false,
        dangerAlert: false

    },

    methods: {
        btnSubmit: function () {
            if (this.firstName === '' || this.lastName === '' || this.dob === '' || this.email === '' || this.homePhone === ''
                || this.address === '' || this.city === '' || this.state === '' || this.zipcode === '' || this.gender === '') {
                this.dangerAlert = true;
                this.successAlert = false;
                return;
            }
            $("form").submit();
        },

        sendEmail: function () {
            alert("ddd");
            var self = this;
            $.post("/login/accountsetup", { email: this.email }, function (result) {
                if (result === false) {
                    self.dangerAlert = true;
                }
            });

        },
    }
});

new Vue({
    el: '#accountSetup',
    data: {
        email: '',
        dangerAlert: false,
        alert: ''
    },

    methods: {
        sendEmail: function () {
            var self = this;
            $.post("/login/accountsetup", { email: this.email }, function (result) {
                if (result.error === false) {
                    self.alert = result.message;
                    self.dangerAlert = true;
                } else {
                    window.location.href = "/login/login";
                }
            });

        },
    }
});