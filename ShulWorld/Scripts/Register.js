﻿new Vue({
    el: '#app',
    data: {
        username: '',
        password: '',
        passwordMatch: ''
    },


    methods: {
        isavailable: function () {
            var self = this;
            $.get("/login/checkusernameavailability", { username: this.username }, function (result) {
                if (result === false || self.username === '' || self.username.length < 5) {
                    $("#usernames").removeClass('has-success').addClass('has-error');
                    $("#check-input").hide();
                    $("#ex-input").show();
                } else {
                    $("#usernames").addClass("has-success").removeClass('has-error');
                    $("#check-input").show();
                    $("#ex-input").hide();
                }
            });
        },

        match: function () {
            if (this.password === this.passwordMatch) {

                $(".passwords").addClass("has-success").removeClass('has-error');
                $(".isMatch").show();
                $(".isNotMatch").hide();
            } else {
                $(".passwords").removeClass('has-success').addClass('has-error');
                $(".isMatch").hide();
                $(".isNotMatch").show();
            }
        },

        btnSubmit: function () {
            if (this.password !== this.passwordMatch || $("#usernames").hasClass('has-error')) {
                $('h2').text("Please fill out incomplete fields");
            } else {
                $.post("/login/register", { username: this.username, password: this.password, passwordMatch: this.passwordMatch }, function () {

                });
            }

        }

    }


});