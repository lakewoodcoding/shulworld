﻿var vm = new Vue({
    el: "#adminIndex",
    data: {
        showIsMember: false,
        showPledges: false,
        nonMember: false,
        member: false,
        showNewType: false,
        showChoices: false,
        showNewMemberForm: false,
        types: [
        { name: "Membership 2016" },
        { name: "Rav Fun" },
        { name: "Chinese Auction" },
        ],

        members: [
       { names: "Moshe", age: 21 },
       { names: "Boruch", age: 20 },
       { names: "MJ", age: 29 }
        ],
        picked: "",
        newType: "",
        date: "",
        search: "",
        memberId: "",
        address: "",
        contact: "",
        email: "",
        amount: "",
        pledge: {}
    },
    methods: {

        beginPledge: function () {
            this.clear();
            this.showIsMember = true;
        },
        next: function () {
            if (this.picked === "")
            { return };
            if (this.picked === "Member") {
                this.member = true;
                this.showIsMember = false;
                return;
            }
            this.showIsMember = false;
            this.nonMember = true;
        },
        submitNonMemberInfo: function () {
            if (this.memberId === "" || this.address === "" || this.contact === "" || this.email === "")
            { return };
            this.pledge.guestname = this.memberId;
            this.pledge.address = this.address;
            this.pledge.contact = this.contact;
            this.pledge.email = this.email;

            this.nonMember = false;
            this.showPledges = true;
        },
        submitMemberInfo: function () {
            if (this.memberId === "")
            { return };
            this.member = false;
            this.showPledges = true;
        },
        createNewType: function () {
            this.showNewType = true;
        },
        searchBox: function () {
            if (this.search !== "") {
                this.showChoices = true;
            }
            else {
                this.showChoices = false;
            }
        },
        submitNewType: function () {
            if (this.newType === "" || this.date === "")
            { return };
            var self = this;
            $.post("/admin/newevent", { Name: self.newType, EventDate: self.date });
            this.showNewType = false;
        },
        submitPledge: function () {
            if (this.amount === "" || this.newType === "")
            { return };
            this.pledge.amount = this.amount;
            this.clear();
        },
        createNewMember: function () {
            this.showNewMemberForm = true;
        },
        clear: function () {
            this.showNewMemberForm = false;
            this.showIsMember = false,
            this.showPledges = false,
            this.nonMember = false,
            this.member = false,
            this.showNewType = false,
            this.showChoices = false
            this.search = "";
            this.memberId = "";
            this.address = "";
            this.contact = "";
            this.email = "";
            this.pledge = {};
            this.newType = "";
            this.date = "";
            this.picked = "";
            this.amount = "";
        },
        getMembers: function () {
            $.get("/admin/getMembers", function (result) {

            })

        },
        getEvents: function () {
            $.get("/admin/getEvents", function (result) {

            })
        },
    }
})