﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shul.Data;
using ShulWorld.Models;

namespace ShulWorld.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public ActionResult Index()
        {
            var memberRepo = new MembersRepository(Properties.Settings.Default.ConStr);
            var userRepo = new UserRepository(Properties.Settings.Default.ConStr);
            var user = userRepo.CheckIfAvailable(User.Identity.Name);
            var memberInfo = memberRepo.GetMemberById(user.MemberId);
            
            return View(memberInfo);
        }

    }
}
