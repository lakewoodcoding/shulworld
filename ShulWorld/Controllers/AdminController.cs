﻿using Shul.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShulWorld.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Events()
        {
            return View();
        }
        public void NewEvent(Event e)
        {
            var repo = new PledgesRepository(Properties.Settings.Default.ConStr);
            repo.CreateEvent(e);
        }
        public ActionResult GetEvents()
        {
            var repo = new PledgesRepository(Properties.Settings.Default.ConStr);
            return Json(new { Events = repo.GetEvents() }, JsonRequestBehavior.AllowGet);
        }

    }
}
