﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Shul.Data;
using ShulWorld.Models;

namespace ShulWorld.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult LogIn()
        {
            var viewModel = new LogInViewModel();
            viewModel.SuccessMessage = (string)TempData["successMessage"];
            viewModel.ErrorMessage = (string)TempData["errorMessage"];
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult LogIn(string username, string password)
        {
            var userRepo = new UserRepository(Properties.Settings.Default.ConStr);
            var user = userRepo.Login(username.ToLower(), password);
            if (user == null)
            {
                TempData["errorMessage"] = "Username or password invalid";
                return Redirect("/login/login");
            }
            FormsAuthentication.SetAuthCookie(user.Username, true);
            return Redirect("/account/index");
        }

        //public ActionResult Register()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Register(string username, string password, string passwordMatch)
        //{
        //    var userRepo = new UserRepository(Properties.Settings.Default.ConStr);
        //    if (userRepo.CheckIfAvailable(username) != null)
        //    {
        //        return View((object)"Username is taken!");
        //    }
        //    if (password != passwordMatch)
        //    {
        //        return View((object)"Passwords do not match!");
        //    }
        //   // userRepo.AddUser(username, password);
        //    TempData["successMessage"] = "You may now login";
        //    return Redirect("/login/login");
        //}

        //public ActionResult CheckUsernameAvailability(string username)
        //{
        //    var userRepo = new UserRepository(Properties.Settings.Default.ConStr);
        //    return Json(userRepo.CheckIfAvailable(username) == null, JsonRequestBehavior.AllowGet);
        //}

        //Authorization for admins only
        [Authorize]
        public ActionResult AddMember()
        {
            var viewModel = new AddMemberViewModel();
            viewModel.NotAddedMessage = (string)TempData["notAddedMessage"];
            viewModel.AddedMessage = (string)TempData["addedMessage"];
            return View(viewModel);
        }

        //Authorization for admins only
        [Authorize]
        [HttpPost]
        public ActionResult AddMember(Member m)
        {
            var memberRepo = new MembersRepository(Properties.Settings.Default.ConStr);
            if (m.FirstName == null || m.LastName == null || m.Address == null || m.Email == null || m.HomePhone == null ||
                m.City == null || m.State == null)
            {
                TempData["notAddedMessage"] = "Missing required fields";
                return Redirect("/login/addmember");
            }
            if (memberRepo.GetMemberByEmail(m.Email) != null)
            {
                TempData["notAddedMessage"] = "That email already has a member assigned to it";
                return Redirect("/login/addmember");
            }
            memberRepo.AddMember(m);
            TempData["addedMessage"] = "Added";
            return Redirect("/login/addmember");
        }

        public ActionResult AccountSetup()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AccountSetup(string email)
        {
            var userRepo = new UserRepository(Properties.Settings.Default.ConStr);
            var memberRepo = new MembersRepository(Properties.Settings.Default.ConStr);
            var asn = new AccountSetupNotifcations();
            var member = memberRepo.GetMemberByEmail(email);
            if (member == null)
            {
                return Json(new {error = false, message= "No member exists with that email"});
            }
            var user = userRepo.CheckIfAvailable(email);
            if (user != null)
            {
                return Json(new { error = false, message = "This member's account has already been setup, contact the administrators at shulworlds@gmail.com" });
            }
            var tempPassword = asn.GenerateTempPassword();
            userRepo.AddUser(email.ToLower(), tempPassword,member.Id);
            asn.SendSetupEmail(email, tempPassword);
            TempData["successMessage"] = "Check your email inbox for login info";
            return Json(true);
        }
    }
}
