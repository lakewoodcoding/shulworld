﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShulWorld.Models
{
    public class AddMemberViewModel
    {
        public string NotAddedMessage { get; set; }
        public string AddedMessage { get; set; }
    }
}