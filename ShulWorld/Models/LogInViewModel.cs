﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShulWorld.Models
{
    public class LogInViewModel
    {
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
    }
}