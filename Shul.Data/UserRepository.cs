﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shul.Data
{
    public class UserRepository
    {
        private string _connectionString;

        public UserRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public User CheckIfAvailable(string username)
        {
            using (var context = new ShulWorldDBDataContext(_connectionString))
            {
                return context.Users.FirstOrDefault(p => p.Username == username);
            }
        }
        public void AddUser(string username, string password,int memberId)
        {
            string salt = PasswordHelper.GenerateRandomSalt();
            string hash = PasswordHelper.HashPassword(password, salt);
            User user = new User
            {
                MemberId = memberId,
                Username = username,
                PasswordHash = hash,
                PasswordSalt = salt
            };

            using (var context = new ShulWorldDBDataContext(_connectionString))
            {
                context.Users.InsertOnSubmit(user);
                context.SubmitChanges();
            }
        }

        public User Login(string username, string password)
        {
            using (var context = new ShulWorldDBDataContext(_connectionString))
            {
                var user = context.Users.FirstOrDefault(u => u.Username == username);
                if (user == null)
                {
                    return null;
                }

                if (!PasswordHelper.PasswordMatch(password, user.PasswordSalt, user.PasswordHash))
                {
                    return null;
                }

                return user;
            }
        }

        public void DeleteUser(int id)
        {
            using (var context = new ShulWorldDBDataContext(_connectionString))
            {
                context.ExecuteCommand("DELETE * FROM Users WHERE Id = {0}", id);
            }
        }
    }
}
