﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shul.Data
{
    public class AccountSetupNotifcations
    {
        public void SendSetupEmail(string email, string password)
        {
            SendEmail("Your email is your username.\n\n Your temporary password is:" + password +
                      "\n\nOnce logged in you can change the password", "Account Activation", email);
        }

        private void SendEmail(string body, string subject,string email)
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.From = new System.Net.Mail.MailAddress("shulworlds@gmail.com");
            message.To.Add(email);
            message.Subject = subject;
            message.Body = body;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Credentials = new System.Net.NetworkCredential("shulworlds", "litclassm04");
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.Send(message);
        }

        public string GenerateTempPassword()
        {
            Random rnd = new Random();
            string tempPassword = "";
            for (int i = 0; i < 4; i++)
            {
                tempPassword += rnd.Next(10, 100);
            }
            return tempPassword;
        }
    }
}

