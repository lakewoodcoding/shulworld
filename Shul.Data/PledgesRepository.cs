﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shul.Data
{
    public class PledgesRepository
    {
        private string _constr;
        public PledgesRepository(string conStr)
        {
            _constr = conStr;
        }
        public void SubmitPledge(Pledge p)
        {
            using (var context = new ShulWorldDBDataContext(_constr))
            {
                context.Pledges.InsertOnSubmit(p);
                context.SubmitChanges();
            }
        }
        public void MakePayment(Payment p)
        {
            using (var context = new ShulWorldDBDataContext(_constr))
            {
                context.Payments.InsertOnSubmit(p);
                context.SubmitChanges();
            }
        }
        public void CreateEvent(Event e)
        {
            using (var context = new ShulWorldDBDataContext(_constr))
            {
                context.Events.InsertOnSubmit(e);
                context.SubmitChanges();
            }
        }
        public IEnumerable<Event> GetEvents()
        {
            using(var context = new ShulWorldDBDataContext(_constr))
            {
                return context.Events.ToList();
            }
        }

    }
}
