﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shul.Data
{
    public class MembersRepository
    {
        private string _constr;
        public MembersRepository(string conStr)
        {
            _constr = conStr;
        }
        public void AddMember(Member m)
        {
            using (var context = new ShulWorldDBDataContext(_constr))
            {
                context.Members.InsertOnSubmit(m);
                context.SubmitChanges();
            }
        }
        public void AddFamilyMember(FamilyMember fm)
        {
            using (var context = new ShulWorldDBDataContext(_constr))
            {
                context.FamilyMembers.InsertOnSubmit(fm);
                context.SubmitChanges();
            }
        }
        public IEnumerable<Member> GetMembers()
        {
            using (var context = new ShulWorldDBDataContext(_constr))
            {
                var loadoptions = new DataLoadOptions();
                loadoptions.LoadWith<Member>(p => p.FamilyMembers);
                context.LoadOptions = loadoptions;
                return context.Members.ToList();
            }
        }


        public Member GetMemberByEmail(string email)
        {
            using (var context = new ShulWorldDBDataContext(_constr))
            {
                return context.Members.FirstOrDefault(m => m.Email.ToLower() == email.ToLower());
            }
        }

        public Member GetMemberById(int id)
        {
            using (var context = new ShulWorldDBDataContext(_constr))
            {
                var loadoptions = new DataLoadOptions();
                loadoptions.LoadWith<Member>(p => p.FamilyMembers);
                context.LoadOptions = loadoptions;
                return context.Members.FirstOrDefault(m => m.Id == id);
            }
        }
    }
}
